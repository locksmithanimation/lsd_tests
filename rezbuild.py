import os
import os.path
import shutil
import stat


def build(source_path, build_path, install_path, targets):

    folders = [x for x in next(os.walk(source_path))[1]
               if x not in ["build", "resources"] and not x.startswith(".")]

    def _build():
        for name in folders:
            src = os.path.join(source_path, name)
            dest = os.path.join(build_path, name)

            if os.path.exists(dest):
                shutil.rmtree(dest)
                
            shutil.copytree(src, dest)


    def _install():
        for name in folders:
            src = os.path.join(build_path, name)
            dest = os.path.join(install_path, name)

            if os.path.exists(dest):
                try:
                    shutil.rmtree(dest)
                except WindowsError:
                    pass
                    
            print("{src}\n =>  {dest}".format(src=src, dest=dest))
            shutil.copytree(src, dest)

    _build()

    if "install" in (targets or []):
        _install()
