name = "lsd_tests"

version = "1.0.0"

description = \
    """
    LSD tests
    """

tools = []

requires = []

tests = {
    "unit": "python -m unittest discover -v -s {root}/python/tests",
    "requires": ["locksmith",
            "edit_tools",
            "sequence_tools",
            "shotgun_tools",
            "vlc",
            "ffmpeg",
            "media_tools",
            "maya_rigging",
            "enum"]
}

uuid = "locksmith_lsd_tests"

def commands():
    env.PYTHONPATH.append("{root}/python")
